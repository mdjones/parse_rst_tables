# Parse rst tables

Some code that aims to efficiently parse very large files (millions of lines)
and extract tables as pandas dataframes.

The tables are in rst-like format:
```
|------------|------------|
|    col 1   |    col 2   |
|------------|------------|
|      1     |    foo     |
|      2     |    bar     |
|------------|------------|
```

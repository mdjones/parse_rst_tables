# We want to parse rst tables, converting dates to datetimes and
# integer numbers to floats, and ending up with a pandas dataframe
import random
import re
from datetime import datetime, timedelta
from io import StringIO
from typing import Iterator, List

import pandas as pd

ROW_WIDTH = 28
BORDER_ROW = f"|{''.join(['-']*ROW_WIDTH)}|{''.join(['-']*ROW_WIDTH)}|\n"
HEADER = f"|{'dates'.center(ROW_WIDTH)}|{'nums'.center(ROW_WIDTH)}|\n"
FILLER_LINES = (
    "\n",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod\n",
    "# Some kind of comment.\n",
)
TAG = "          <TAG>    \n"
NON_TABLE_PATTERN = r"^\|-.*$|^[^|].*$"


def create_rst_table(n_rows=100) -> str:
    dates_values = pd.date_range(
        datetime.now(),
        datetime.now() + timedelta(days=1.0),
        freq=timedelta(hours=24.0 / n_rows),
    )
    dates_values_str = [
        date_value.isoformat(" ", "seconds").center(ROW_WIDTH)
        for date_value in dates_values
    ]
    nums_values_str = [
        str(random.randrange(0, 100)).center(ROW_WIDTH) for _ in range(n_rows)
    ]
    value_rows = [
        f"|{value_row[0]}|{value_row[1]}|\n"
        for value_row in zip(dates_values_str, nums_values_str)
    ]
    return "".join([BORDER_ROW, HEADER, BORDER_ROW] + value_rows + [BORDER_ROW])


def create_file_contents(table_size: int = 100_000) -> str:
    """
    The file contains multiple tables with random other lines in-between.
    There is a tag of some sort before each table.
    """
    return (
        f"{_generate_non_table_lines()}{TAG}{_generate_non_table_lines()}{create_rst_table(table_size)}"
        f"{_generate_non_table_lines()}{TAG}{_generate_non_table_lines()}{create_rst_table(table_size)}"
        f"{_generate_non_table_lines()}{TAG}{_generate_non_table_lines()}{create_rst_table(table_size)}"
        f"{_generate_non_table_lines()}"
    )


def _generate_non_table_lines() -> str:
    number_of_non_table_lines = random.randrange(1, 20)
    return "".join(
        [
            FILLER_LINES[random.randrange(0, len(FILLER_LINES))]
            for _ in range(number_of_non_table_lines + 1)
        ]
    )


def read_file_contents_for_benchmark(file_contents: str) -> List[pd.DataFrame]:
    return list(read_file_contents(file_contents))


def read_file_contents(file_contents: str) -> Iterator[pd.DataFrame]:
    """
    Using splitlines and string matching may look a little more straightforward
    than identifying the table content using regex, however splitting and
    then rejoining to call read_csv is significantly slower.
    """
    split_file = re.split(TAG, file_contents)
    for table_chunk in split_file[1:]:  # "1:" as there is no table before the first tag
        yield read_table(table_chunk)


def read_table(rst_table_content_and_surrounding_lines: str) -> pd.DataFrame:
    rst_table_content = _remove_non_table_lines(rst_table_content_and_surrounding_lines)
    df = pd.read_csv(
        StringIO(rst_table_content),  # type: ignore
        delimiter="|",
        skipinitialspace=True,  # reduces run time significantly
    )
    df = _drop_empty_columns(df)
    _strip_whitespace_from_column_names(df)
    _convert_to_datetimes_and_int_to_float(df)
    return df


def _remove_non_table_lines(rst_table_content_and_surrounding_lines: str) -> str:
    """
    Remove lines which do not start with "|"
    or start with "|-" (table border lines)
    """
    return re.sub(
        NON_TABLE_PATTERN,
        "",
        rst_table_content_and_surrounding_lines,
        flags=re.MULTILINE,
    )


def _drop_empty_columns(df_with_empty_columns: pd.DataFrame) -> pd.DataFrame:
    """
    Drop empty/unnamed columns which are due to the extra delimiter
    at the start and end of each line. This is cheaper than slicing
    off the extra delimiters before calling read_csv.
    """
    return df_with_empty_columns.loc[
        :, ~df_with_empty_columns.columns.str.contains("^Unnamed")
    ]


def _convert_to_datetimes_and_int_to_float(df):
    for column in df.columns:
        if pd.api.types.infer_dtype(df[column]) == "string":
            df[column] = pd.to_datetime(df[column], errors="ignore")
        if pd.api.types.infer_dtype(df[column]) == "integer":
            df[column] = df[column].astype(float)


def _strip_whitespace_from_column_names(df):
    df.columns = df.columns.str.strip()


def test_read_table(benchmark):
    table = create_rst_table(100_000)
    # mean <190ms on my home machine
    df = benchmark(read_table, table)
    assert pd.api.types.infer_dtype(df[df.columns[0]]) == "datetime64"
    assert pd.api.types.infer_dtype(df[df.columns[1]]) == "floating"


def test_read_file(benchmark):
    fake_file_content = create_file_contents(100_000)
    dfs = benchmark(read_file_contents_for_benchmark, fake_file_content)
    for df in dfs:
        assert pd.api.types.infer_dtype(df[df.columns[0]]) == "datetime64"
        assert pd.api.types.infer_dtype(df[df.columns[1]]) == "floating"


if __name__ == "__main__":
    f_content = create_file_contents(100_000)
    for table_num, output_df in enumerate(read_file_contents(f_content)):
        print(f"Table {table_num}:\n{output_df}\n")
